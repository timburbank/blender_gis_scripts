# QGIS: Reprojecting Points for Blender

The scripts which deal with point data, expect a delimited text file, with XYZ coordinates, in "local space" ie with the origin somewhere in the middle. QGIS [qgis.org](https://qgis.org/) is an open source and cross platform tool that will do this nicely.

## Add Data to Project

Import the data `Layer -> Add Layer -> Add Delimited Text Layer...` (or whatever the data type is)

It usually makes life easier to make sure you're working in geographic coordinates (lat/lon). Go to `Project -> Properties -> CRS` and choose WGS 84, EPSG:4326.

## Create Local Projection

(same as `qgis_reprojecting_heightmaps_for_blender.md`)

We need a custom projection in QGIS that describes geospacial locations with a meter grid centered on a specific latitude and longitude at sea level. The origin point of this projection will match the origin in Blender, so it's good to pick a point near the center of your data set. If you're adding multiple data sets to the same Blender project, use the same projection for all of them, and they'll match up.


Go to `Settings -> Custom Projections...` 

Green plus sign to create a new layer

Name it something helpful

Set Format to Proj String

In Parameters, enter the following string, replacing LAT and LON with the latitude and longitude of the origin

    +proj=tmerc +lat_0=LAT +lon_0=LON +units=m +ellps=WGS84 +datum=WGS84


- `+proje=tmerc` specifies Transverse Mercator which behaves as a simple xyz grid over short distances
- `+units=m` to use meters
- `+ellps=WSG84 and +datum=WSG84` use the WSG84 ellipsoid, which is the one everyone uses

![Custom CRS dialog](images/qgis_custom_crs.png)


### For BlenderGIS Compatibility

Webmap data imported through [BlenderGIS](https://github.com/domlysz/BlenderGIS) is usually in Web Mercator projection, so you'll want to use the following projection instead

    +proj=webmerc  x_0=X_OFFSET y_0=Y_OFFSET +ellps=WGS84 +datum=WGS84

X_OFFSET and Y_OFFSET should be the negative of the projection origin used by BlenderGIS, shown in the 3D Viewport Sidebar (hit N in 3D Viewport)

![BlenderGIS origin panel](images/blendergis_origin.jpg)

## Save Data in New Projection

Right click on layer in QGIS and choose `Export -> Save Feature As...`

- Format as CSV
- Set file name
- For CRS choose the custom projection you just created
- Under Layer Options, set GEOMETRY to AS_XYZ
  - This will add X, Y, and Z columns in front of the data, and keep the original data for each row after that
- SEPARATOR doesn't matter, just set the same thing in the importer script (but TAB is better, just on principal)

![Save Vector Layer As dialog](images/qgis_save_vector_layer_as.png)
