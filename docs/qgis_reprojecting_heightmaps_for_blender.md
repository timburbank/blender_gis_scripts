# QGIS: Reprojecting Heightmaps for Blender

To import georeferenced gridded heightmap data (such as floating point GeoTIFFs) into Blender with `mesh_from_xyz_heightmap.py`, it needs to be reprojected into local coordinate space, and saved to an ASCII XYZ file. QGIS [qgis.org](https://qgis.org/) is an open source and cross platform tool that will do this nicely.

## Add Data to Project

Import the data (can drag and drop most gridded data formats into QGIS)

It usually makes life easier to make sure you're working in geographic coordinates (lat/lon). Go to `Project -> Properties -> CRS` and choose WGS 84 (EPSG:4326).


## Create Local Projection

(same as `qgis_reprojecting_points_for_blender.md`)

We need a custom projection in QGIS that describes geospacial locations with a meter grid centered on a specific latitude and longitude at sea level. The origin point of this projection will match the origin in Blender, so it's good to pick a point near the center of your data set. If you're adding multiple data sets to the same Blender project, use the same projection for all of them, and they'll match up.


Go to `Settings -> Custom Projections...` 

- Green plus sign to create a new layer
- Name it something helpful
- Set Format to Proj String
- In Parameters, enter the following string, replacing LAT and LON with the latitude and longitude of the origin

    +proj=tmerc +lat_0=LAT +lon_0=LON +units=m +ellps=WGS84 +datum=WGS84

- `+proje=tmerc` specifies Transverse Mercator which behaves as a simple xyz grid over short distances
- `+units=m` to use meters
- `+ellps=WSG84 and +datum=WSG84` use the WSG84 ellipsoid, which is the one everyone uses

![Custom CRS dialog](images/qgis_custom_crs.png)

### For BlenderGIS Compatibility

Webmap data imported through [BlenderGIS](https://github.com/domlysz/BlenderGIS) is usually in Web Mercator projection, so you'll want to use the following projection instead

    +proj=webmerc  x_0=X_OFFSET y_0=Y_OFFSET +ellps=WGS84 +datum=WGS84

X_OFFSET and Y_OFFSET should be the negative of the projection origin used by BlenderGIS, shown in the 3D Viewport Sidebar (hit N in 3D Viewport)

![BlenderGIS origin panel](images/blendergis_origin.jpg)

## Reproject Grid

Select the data layer and choose `Raster -> Projections -> Warp (Reproject)...`

![Warp Menu](images/raster_projections_warp.png)

- Set the Source CRS to the existing projection of the data
  - Usually this is plain WGS 84 (EPSG:4326) ie. Geographic ie. latitude and longitude
- Set the Target CRS to the new projection (note: may have to click the globe button next to the dropdown)
- The default resampling method "Nearest Neighbor" can give blocky artifacts. Cubic has better results.
- If there are holes in the data, set Nodata value to -9999 (`mesh_from_xyz_heightmap.py` recognizes -9999 or non-numbers as nodata)
- Hit Run to save the result as a temporary file and add it to QGIS project

![Reproject Dialog](images/qgis_reproject_settings.png)


## Save to ASCII XYZ

With the reprojected layer selected, go to `Raster -> Conversion -> Translate (Convert Format)...`

![Raster Translate Menu](images/qgis_raster_translate.png)

Change the Converted output from temporary file to "Save to a File.." and give your output file an xyz extension

![Save as XYZ dialog](images/qgis_save_as_xyz.png)