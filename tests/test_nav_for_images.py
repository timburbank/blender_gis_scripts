import unittest
import os
import sys
import logging
from datetime import datetime
from datetime import timedelta

logging.basicConfig(
    format='%(levelname)s %(funcName)s %(lineno)s: %(message)s', 
    level=logging.DEBUG)

current_dir = os.path.dirname(os.path.realpath(__file__))
parent_dir = os.path.dirname(current_dir)
data_dir = os.path.join(current_dir, 'test_data')

sys.path.insert(0, parent_dir) 
from nav_for_images import NavImageMatcher
from nav_for_images import collect_image_paths


class TestNavForImages(unittest.TestCase):
    nav_matcher = NavImageMatcher(
        file_time_format='%Y%m%d_%H%M%S',
        timestamp_column=0,
        data_time_format='%Y-%m-%dT%H:%M:%SZ',
        min_time_diff=timedelta(seconds=0))

    def test_associate_data_with_filenames(self):
        test_img_paths = [
            '/meep/morp/20210307_000010.png',
            '/meep/morp/20210307_000020.png',
        ]

        test_nav_records = [
            '2021-03-07T00:00:08Z\t0\n',
            '2021-03-07T00:00:10Z\t1\n',
            '2021-03-07T00:00:15Z\t2\n',
            '2021-03-07T00:00:23Z\t3\n'
        ]

        expected_results = [
            '2021-03-07T00:00:10Z\t1\t/meep/morp/20210307_000010.png',
            '2021-03-07T00:00:23Z\t3\t/meep/morp/20210307_000020.png',
        ]

        results = self.nav_matcher.associate_data_with_filenames(
            test_img_paths,
            test_nav_records)

        result_list = [result for result in results]
        self.assertEqual(result_list, expected_results)


    def test_find_closest_record(self):
        test_nav_records = [
            '2021-03-07T00:00:08Z\t0',
            '2021-03-07T00:00:10Z\t1',
            '2021-03-07T00:00:15Z\t2',
        ]

        ## Test exact match
        target_time = datetime(2021, 3, 7, 0, 0, 10)
        result = self.nav_matcher.find_closest_record(
            target_time,
            test_nav_records)

        self.assertEqual(result, '2021-03-07T00:00:10Z\t1')

        ## Test close match
        target_time = datetime(2021, 3, 7, 0, 0, 14)
        result = self.nav_matcher.find_closest_record(
            target_time,
            test_nav_records)

        self.assertEqual(result, '2021-03-07T00:00:15Z\t2')


    def test_collect_image_paths(self):
        search_dir = os.path.join(data_dir, 'collect_image_paths')
        extensions = ['.png', '.jpg']
        expected_results = [
            os.path.join(search_dir, 'file1.png'),
            os.path.join(search_dir, 'file2.jpg'),
            os.path.join(search_dir, 'subdir', 'file3.png')
        ]

        results = collect_image_paths(search_dir, extensions)

        self.assertEqual(set(results), set(expected_results))


if __name__ == '__main__':
    unittest.main()