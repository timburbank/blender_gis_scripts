#/usr/bin/python3
'''
Run from command line (not in Blender)

Associate images with navigation data, to prepare
input to image_plans_at_locations.py

Reads directory of images with timestamped filenames 
(ie. cam1_20210503000934.png) searches for the closest 
record in a delimited text file with timestamped locations
(ie. "2021-05-03T00:11:02Z, 1203.321, -15012.121, -3104.032")
'''

import argparse
import os
import logging
from datetime import datetime
from datetime import timedelta


class NavImageMatcher(object):
    def __init__(self, file_time_format, timestamp_column, data_time_format, min_time_diff=0, data_record_separator='\t'):
        self.file_time_format = file_time_format
        self.timestamp_column = timestamp_column
        self.data_time_format = data_time_format
        self.min_time_diff = min_time_diff
        self.data_record_separator = data_record_separator


    def associate_data_with_filenames(self, image_paths, data_records):
        results = []

        for image_path in image_paths:
            try:
                image_time = datetime.strptime(
                    os.path.splitext(os.path.basename(image_path))[0],
                    self.file_time_format)
            except ValueError:
                logging.warning('Skipping {}'.format(os.path.basename(image_path)))

            best_record = self.find_closest_record(image_time, data_records)

            yield '{}{}{}'.format(
                best_record.strip(),
                self.data_record_separator,
                image_path)

        return results


    def find_closest_record(self, target_time, data_records):
        '''
        Given datetime and list of data record strings, return 
        the data record with a timestamp closest to the target
        '''
        best_record_delta = None

        for data_record in data_records:
            data_fields = data_record.split(self.data_record_separator)
            data_record_time = datetime.strptime(
                data_fields[self.timestamp_column],
                self.data_time_format)

            delta_from_target = abs(data_record_time - target_time)

            if best_record_delta is None:
                best_record = data_record
                best_record_delta = delta_from_target

            elif delta_from_target < best_record_delta:
                best_record = data_record
                best_record_delta = delta_from_target

        return best_record



def collect_image_paths(search_dir, extensions):
    '''
    Recursively search directory for files with matching
    extensions and return a list of all matching absolute paths
    '''
    results = []
    for root, dirs, filenames in os.walk(search_dir):
        for filename in filenames:
            if os.path.splitext(filename)[1] in extensions:
                results.append(os.path.join(root, filename))

    return results


def parse_cli_args():
    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        'image_dir',
        help='Directory to recursively search for image files')

    parser.add_argument(
        'csv_file',
        type=argparse.FileType(),
        help='Delimited text file with timestamped data')

    parser.add_argument(
        '--file-time-format',
        '-f',
        default='cam1_%Y%m%d%H%M%S',
        help='Format of timestamp in image filename. Default: cam1_%%Y%%m%%d%%H%%M%%S')

    parser.add_argument(
        '--timestamp-column',
        '-c',
        type=int,
        default=0,
        help='Column of CSV with timestamp. Default 0')

    parser.add_argument(
        '--data-time-format',
        '-t',
        default='%Y-%m-%dT%H:%M:%S',
        help='Format of timestamp in CSV file. Default: %%Y-%%m-%%dT%%H:%%M:%%S')

    parser.add_argument(
        '--extensions',
        '-e',
        nargs='+',
        default=['.png', '.jpg', '.tif'],
        help='Image file extensions to include, default: .png, .jpg, .tif')

    parser.add_argument(
        '--skip-headers',
        '-s',
        type=int,
        default=0,
        help='Number of header lines to skip, default: 0')

    parser.add_argument(
        '--delimiter',
        '-d',
        default='\t',
        help='CSV delimiter')

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_cli_args()
    nav_matcher = NavImageMatcher(
        file_time_format=args.file_time_format,
        timestamp_column=args.timestamp_column,
        data_time_format=args.data_time_format,
        min_time_diff=timedelta(seconds=0),
        data_record_separator=args.delimiter)


    image_paths = collect_image_paths(args.image_dir, args.extensions)
    data_records = args.csv_file.readlines()[args.skip_headers:]

    results = nav_matcher.associate_data_with_filenames(
        image_paths, 
        data_records)

    for result in results:
        print(result)