'''
mesh_from_xyz_heightmap.py
Create mesh from ASCII XYZ gridded heightmap

Expects a whitespace delimited text file where each row contains
X, Y, Z coordinates for one grid cell in local coordinate space. 
Non numerical values will be skipped, leaving holes in the mesh.

Data can be prepared with QGIS, see:
docs/qgis_reprojecting_heightmaps_for_blender.md

This script assumes all cells in a row are adjacent to each other in the file

- Copy and paste this file into Blender's text editor
- Set parameters below
- Run script
'''

import bpy
import os

DATA_PATH = r'path/to/data.xyz'

## Used if grid cells aren't aliged along X axis. Ignored if None.
## Set width (x) in cells of gridded data file
WIDTH = None

## Scale coordinates before creating mesh
SCALE = 0.01

## Use every n cells
SUBSAMPLE = 1

## Add to Z values before creating mesh
Z_OFFSET = 0

data = []
new_row = []
last_y = None
row_start = 0
source_row_index = 0
with open(DATA_PATH) as bathy_file:
    col_i = 0
    print('Reading file...')
    for i, line in enumerate(bathy_file):
        line_data = line.split()
        
        if WIDTH is not None:
            if i % WIDTH == 0 and i != 0:
                data.append(new_row)
                new_row = []
                row_start = i
                source_row_index += 1
                    
        else:
            if line_data[1] != last_y:
                if new_row != []:
                    data.append(new_row)
                    new_row = []
                row_start = i
                source_row_index += 1
                
            last_y = line_data[1]


            
        if (i - row_start) % SUBSAMPLE == 0 and source_row_index % SUBSAMPLE == 0:
            new_row.append({
                'x': float(line_data[0]),
                'y': float(line_data[1]),
                'z': float(line_data[2])})

            

print('Adding verts...')
verts = []
vert_indexes = []
vi = 0
for row in data:
    vi_row = []
    for col in row:
        ## Handle -9999 nodata values
        if col['z'] == -9999:
            vi_row.append(None)
            continue
        try:
            verts.append((
                col['x'] * SCALE, 
                col['y'] * SCALE, 
                (col['z'] + Z_OFFSET) * SCALE))
            vi_row.append(vi) ## I think this saves the literal value?
            vi += 1
        except TypeError: ## Handle NaNs
            vi_row.append(None)
            continue
    vert_indexes.append(vi_row)

print('Adding faces...')
faces = []
for row_i in range(0, len(vert_indexes) - 1):
    for col_i in range(0, len(vert_indexes[row_i]) - 1):
        try:
            new_face = (
                vert_indexes[row_i + 1][col_i],
                vert_indexes[row_i + 1][col_i + 1],
                vert_indexes[row_i][col_i + 1],
                vert_indexes[row_i][col_i])
                
            if None not in new_face:
                faces.append(new_face)

        except IndexError: ## shouldn't need this since points exist but with None value
            pass                


name = os.path.splitext(os.path.basename(DATA_PATH))[0]
bathy_mesh = bpy.data.meshes.new(name)
bathy_mesh.from_pydata(verts, [], faces)

# Create Object and link to scene 
bathy_obj = bpy.data.objects.new(name, bathy_mesh)
bpy.context.scene.collection.objects.link(bathy_obj)

# Select the object 
bpy.context.view_layer.objects.active = bathy_obj 
print('Done')
