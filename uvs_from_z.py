'''
Assign UV coordinates for each vertex based on it's Z location.

This can be useful for UV mapping a mesh's height to a color
ramp image, if the mesh is to large to easily UV unwrap normally.
(Sets both X and Y UV coordinates so it will work with either
horizontal or vertical color map gradient images)

- Copy and paste this file into Blender's text editor
- Set OBJECT_NAME below to the target object
- Run script

Will output progress to terminal if Blender was run
from command line

Tested with Blender 2.92
'''

import bpy

OBJECT_NAME = 'Cube'

print('Setting UVs...')

ob = bpy.data.objects[OBJECT_NAME]
bound_box_min_z = min([coord[2] for coord in ob.bound_box])
bound_box_max_z = max([coord[2] for coord in ob.bound_box])
bound_box_height = bound_box_max_z - bound_box_min_z
num_faces = len(ob.data.polygons)

## If we don't have a UV layer create one
if ob.data.uv_layers.active is None:
    ob.data.uv_layers.new()

for face in ob.data.polygons:
    print('Face {}, {:.0f}%'.format(face.index, face.index * 100 / num_faces ))

    for vert_idx, loop_idx in zip(face.vertices, face.loop_indices):
        uv_coords = ob.data.uv_layers.active.data[loop_idx].uv
        vert_z = ob.data.vertices[vert_idx].co.z
        vert_z_normalized = (vert_z - bound_box_min_z) / bound_box_height

        ## To invert mapping, use: uv_coords.y = 1 - vert_z_normalized
        uv_coords.y = vert_z_normalized
        uv_coords.x = vert_z_normalized