'''
anim_from_csv.py

Read a delimited text file with XYZ coordinates and add
position keyframes to an object. Expects coordinates to be
at consistent intervals. Keyframes will be evenly
distributed between START_FRAME and END_FRAME

- Copy and paste this file into Blender's text editor
- Set parameters below
- Run script
'''

import bpy
import csv

START_FRAME = 1
END_FRAME = 250

DATA_FILE = r'path/to/file.csv'
 
## Parameters to parse file
X_COL = 0
Y_COL = 1
Z_COL = 2
DELIMITER = ','
SKIP_HEADER_LINES = 1

## Read only every nth row
# SUBSAMPLE = 1

## Multiply every coordinate value by this amount
SCALE = 0.01

## Multiply Z coordinate by this amount (after scaling)
## Set to -1 for z-positive down
Z_MULTIPLIER = 1

## API path to object that will be animated 
TARGET = bpy.data.objects['meep']


coords = []

print('reading file...')
with open(DATA_FILE) as in_file:
    reader = csv.reader(in_file, delimiter=DELIMITER)
    for i, row in enumerate(reader):
        if i <= SKIP_HEADER_LINES:
            continue

        coords.append((
            float(row[X_COL]) * SCALE, 
            float(row[Y_COL]) * SCALE, 
            float(row[Z_COL]) * SCALE * Z_MULTIPLIER))

print('adding keyframes...')
last_frame = 0
for i, coord in enumerate(coords):
    frame = int( ((i + 1) / len(coords)) * (END_FRAME - START_FRAME) + START_FRAME)
    if frame == last_frame:
        continue
    
    last_frame = frame
    
    TARGET.location = coord
    TARGET.keyframe_insert(data_path="location", frame=frame)

print('anim_from_csv.py done')