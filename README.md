# blender_gis_scripts

This is a collection of Blender Python scripts and processes for importing different kinds of geospacial data into Blender, while preserving georeferencing.

Blender doesn't speak geospacial, so "preserving georeferencing" here means saying Blender's origin is at a specific location on Earth, and then converting all coordinates (latitude/longitude, UTM zones, etc.) into Blender's coordinate system. This is compatible with [BlenderGIS](https://github.com/domlysz/BlenderGIS) tools, as long as the origin point and projection are kept consistent.

The actual GIS processing is done externally, in QGIS [qgis.org](https://qgis.org/), which is open source, cross platform, and generally awesome. The scripts here just read the reprojected data from QGIS, and create various kinds of Blender objects.

For directions on pre-processing data in QGIS, see:

- [docs/qgis_reprojecting_points_for_blender.md](docs/qgis_reprojecting_points_for_blender.md) for point data, such as location markers or navigation
- [docs/qgis_reprojecting_heightmaps_for_blender.md](docs/qgis_reprojecting_heightmaps_for_blender.md) for raster heightmaps



## `anim_from_csv.py`

Addes location animation to an object from coordinates in a delimited text file.

![Animation Curves](docs/images/anim_curves.jpg)


## `collection_instance_from_csv_points.py`

For adding location markers to a scene. Creates instances of a collection at each point listed in a delimited text file.

![Waypoints in Blender](docs/images/waypoints.jpg)

## `curve_from_csv_points.py`

Creates a curve object connecting points defined in a delimited text file. Used for visualizing navigation paths.

![Navigation track line in Blender](docs/images/trackline.jpg)

## `image_planes_at_locations.py`

If you have a set of geolocated images, described in a delimited text file, this will add them to the Blender scene at the correct location.

![Geo located images in Blender](docs/images/geolocated_images.jpg)

## `mesh_from_xyz_heightmap.py`

Used for creating terrain. This takes gridded heightmap data, such as what can be downloaded from [gmrt.org](https://www.gmrt.org/), and creates a mesh with a vertex at each grid cell.

![Bathymetry data in Blender](docs/images/bathymetry.jpg)

## `nav_for_images.py`

This is a helper script to prepare data for `image_planes_at_locations.py`. Correlate images with timestamps in their filenames to a delimited text file with timestamped locations.


## Note about running Blender scripts

Open Blender in a Terminal window (drag the executable into the Terminal and hit enter) to see any error output when running scripts.
