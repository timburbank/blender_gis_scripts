'''
collection_instance_from_csv_points.py

Reads a delimited text file with a list of XYZ coordinates,
and create instances of a collection at each location

- Copy and paste this file into Blender's text editor
- Set parameters below
- Select collection to add new collections into 
- Run script
'''

## Delimited text file, with each row containing an X, Y, and Z coordinates
DATA_FILE = r'V:\pr\oet\2021_local\anim_NA110\data\H1759_waypoints.csv'

## Name of collection to create instances of
COLLECTION_NAME = 'meep'

## Parameters to parse file
X_COL = 0
Y_COL = 1
Z_COL = 2
DELIMITER = '\t'
SKIP_HEADER_LINES = 1

## Use this column as the instance name. 
## If None, will use original collection name + instance number
NAME_COL = None

## Read only every nth row
SUBSAMPLE = 1

## Multiply every coordinate value by this amount
SCALE = 0.01

## Multiply Z coordinate by this amount (after scaling)
## Set to -1 for z-positive down
Z_MULTIPLIER = 1


import bpy
import csv

# sample data
coords = []

source_collection = bpy.data.collections[COLLECTION_NAME]

with open(DATA_FILE) as in_file:
    reader = csv.reader(in_file, delimiter=DELIMITER)
    for i, row in enumerate(reader):
        if i <= SKIP_HEADER_LINES:
            continue

        if i % SUBSAMPLE != 0:
            continue

        if NAME_COL is None:
            instance_name = '{}{:02d}'.format(COLLECTION_NAME, i)
        else:
            instance_name = name=row[NAME_COL]

        instance_obj = bpy.data.objects.new(
            instance_name,
            object_data=None)
            
        instance_obj.instance_collection = source_collection
        instance_obj.instance_type = 'COLLECTION'
        instance_obj.location = (
            float(row[X_COL]) * SCALE, 
            float(row[Y_COL]) * SCALE, 
            float(row[Z_COL]) * SCALE * Z_MULTIPLIER)

        ## Add to an existing collection so it shows up in scene
        parent_collection = bpy.context.view_layer.active_layer_collection
        parent_collection.collection.objects.link(instance_obj)
        
        ## could also link like this?
        #scn = bpy.context.scene
        #scn.collection.objects.link(instance_obj)
        
