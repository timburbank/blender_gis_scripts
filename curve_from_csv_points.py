'''
curve_from_csv_points.py

Reads a delimited text file with a list of XYZ coordinates,
and create a curve object connecting those points

- Copy and paste this file into Blender's text editor
- Set parameters below
- Select collection to add curve into 
- Run script
'''

DATA_FILE = r'path/to/file.csv'
 
## Parameters to parse file
X_COL = 0
Y_COL = 1
Z_COL = 2
DELIMITER = '\t'
SKIP_HEADER_LINES = 0

## Read only every nth row
SUBSAMPLE = 1

## Multiply every coordinate value by this amount
SCALE = 0.01

## Multiply Z coordinate by this amount (after scaling)
## Set to -1 for z-positive down
Z_MULTIPLIER = 1


import bpy
import csv

# sample data
coords = []

with open(DATA_FILE) as in_file:
    reader = csv.reader(in_file, delimiter=DELIMITER)
    for i, row in enumerate(reader):
        if i <= SKIP_HEADER_LINES:
            continue

        if i % SUBSAMPLE != 0:
            continue

        coords.append((
            float(row[X_COL]) * SCALE, 
            float(row[Y_COL]) * SCALE, 
            float(row[Z_COL]) * SCALE * Z_MULTIPLIER))

# create the Curve Datablock
name = bpy.path.display_name_from_filepath(DATA_FILE)
curveData = bpy.data.curves.new(name, type='CURVE')
curveData.dimensions = '3D'
curveData.resolution_u = 2

# map coords to spline
polyline = curveData.splines.new('NURBS')
polyline.points.add(len(coords)-1)
for i, coord in enumerate(coords):
    x,y,z = coord
    polyline.points[i].co = (x, y, z, 1)
    
    
# create Object
curveOB = bpy.data.objects.new(name, curveData)
curveData.bevel_depth = 0.01

# attach to scene and validate context
scn = bpy.context.scene
scn.collection.objects.link(curveOB)
scn.objects.active = curveOB