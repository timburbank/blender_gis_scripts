'''
image_planes_at_locations.py

Take a delimited text file with xyz coordinates and image paths,
and add the images as planes at the coordinate locations

nav_for_images.py can be used to associate timestamped images 
with location data for this script.

- Copy and paste this file into Blender's text editor
- Set parameters below
- Select collection to add image planes into
- Run script
'''

DATA_PATH = r'path/to/data.csv'

## Parameters to parse file
DELIMITER = ','
X_COL = 0
Y_COL = 1
Z_COL = 2
IMG_PATH_COL = 7

## Invert z coordinates
POSITIVE_DOWN = True

## Multiply every coordinate value by this amount
SCALE = 0.01

IMG_PLANE_WIDTH = 5

## Don't add image if it's with min distance of last image location
MIN_IMG_PLANE_DISTANCE = 5



import bpy
import csv
import os
import io_import_images_as_planes
from mathutils import Vector
from bpy_extras.image_utils import load_image


def add_image_plane_at_loc(image_path, loc_x, loc_y, loc_z):
    img_spec = load_image(image_path, check_existing=True)
    height = (IMG_PLANE_WIDTH / img_spec.size[0]) * img_spec.size[1]
    
    verts = [
        [0, 0, 0],
        [0, height, 0],
        [IMG_PLANE_WIDTH, height, 0],
        [IMG_PLANE_WIDTH, 0, 0]]    
    
    edges = []
    faces = [[0, 1, 2, 3]]
    name =  bpy.path.display_name_from_filepath(image_path)
    
    mesh = bpy.data.meshes.new(name=name)
    mesh.from_pydata(verts, edges, faces)
    
    obj = bpy.data.objects.new(name, mesh)
    obj.location = (loc_x, loc_y, loc_z)
    bpy.context.scene.collection.objects.link(obj)
    
    
    ## Add Material
    ## This is heavily based of the Import Images as Planes addon
    ## and uses a number of functions from that module directly
    
    ## Reuse if existing
    material = None
    for mat in bpy.data.materials:
        if mat.name == name:
            material = mat
            
    if not material:
        material = bpy.data.materials.new(name=name)

    material.use_nodes = True
    node_tree = material.node_tree
    out_node = io_import_images_as_planes.clean_node_tree(node_tree)
    
    ## Create texture node
    tex_image = node_tree.nodes.new('ShaderNodeTexImage')
    tex_image.image = img_spec
    tex_image.show_texture = True
    
    core_shader = io_import_images_as_planes.get_shadeless_node(node_tree)
    node_tree.links.new(core_shader.inputs[0], tex_image.outputs[0])
    node_tree.links.new(out_node.inputs[0], core_shader.outputs[0])
    io_import_images_as_planes.auto_align_nodes(node_tree)
    
    obj.data.materials.append(material)


    ## UV unwrap
    ## maybe works better if after material?
    bpy.context.view_layer.objects.active = obj
    bpy.ops.object.mode_set(mode = 'EDIT')
    bpy.ops.mesh.select_all(action = 'SELECT')
    bpy.ops.uv.unwrap()
    bpy.ops.mesh.select_all(action = 'DESELECT')
    bpy.ops.object.mode_set(mode = 'OBJECT')
    
    
def calc_distance(point1: Vector, point2: Vector) -> float:
    return (point2 - point1).length

if __name__ == '__main__':
    with open(DATA_PATH) as data_file:
        data_reader = csv.reader(data_file, delimiter=DELIMITER)
        old_location = None
        if POSITIVE_DOWN:
            depth_multiplier = -1
        else:
            depth_multiplier = 1
        
        for record in data_reader:
            new_location = Vector([
                float(record[X_COL]) * SCALE,
                float(record[Y_COL]) * SCALE,
                float(record[Z_COL]) * SCALE * depth_multiplier])
                
            if old_location and calc_distance(old_location, new_location) < MIN_IMG_PLANE_DISTANCE:
                print(calc_distance(old_location, new_location))
                continue
            
            old_location = new_location
            add_image_plane_at_loc(record[IMG_PATH_COL], *new_location)

